set t_Co=256
set shell=/bin/bash
let $BASH_ENV="~/.bash_vim"
syntax on
set relativenumber
set bg=dark
set nocompatible              " be iMproved, required
set hlsearch
set colorcolumn=110
set cursorcolumn
set cursorline
highlight CursorColumn cterm=NONE ctermbg=233
highlight CursorLine cterm=NONE ctermbg=233 ctermfg=white
highlight ColorColumn ctermbg=darkgrey
highlight HiTab ctermfg=234

nnoremap <silent> <CR> :nohlsearch<Bar>:echo<CR>
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
autocmd InsertEnter * :setlocal nohlsearch
autocmd InsertLeave * :setlocal hlsearch

match HiTab /\t/

set list
set listchars=tab:>-
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-surround'
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdcommenter'
Plugin 'godlygeek/tabular'
Plugin 'Valloric/YouCompleteMe'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'fs111/pydoc.vim'
Plugin 'Rykka/riv.vim'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'easymotion/vim-easymotion'
Plugin 'vim-scripts/a.vim'
Plugin 'bling/vim-airline'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'jeaye/color_coded'
Plugin 'rdnetto/YCM-Generator'
"Plugin 'klen/python-mode'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:ycm_show_diagnostics_ui = 0
let g:syntastic_python_python_exec = '/usr/bin/env python3'

let g:syntastic_c_checkers = ['clang_check']
let g:syntastic_c_compiler = 'clang-check'
let g:syntastic_c_compiler_options = '-std=c11 -Wall -Wextra'
let g:syntastic_c_clang_check_compiler = 'clang-check'
let g:syntastic_c_clang_check_compiler_options = '-std=c11 -Wall -Wextra'
let g:syntastic_c_clang_check_post_args = ""

let g:syntastic_cpp_checkers = ['clang_check']
let g:syntastic_cpp_compiler = 'clang-check'
let g:syntastic_cpp_compiler_options = '-std=c++11 -Wall -Wextra'
let g:syntastic_cpp_clang_check_compiler = 'clang-check'
let g:syntastic_cpp_clang_check_compiler_options = '-std=c++11 -Wall -Wextra'
let g:syntastic_cpp_clang_check_post_args = ""
let g:syntastic_clang_check_config_file = '.syntastic_cpp_config'

" NERDtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" NERDtree Tabs
let g:nerdtree_tabs_open_on_console_startup=1
let g:nerdtree_tabs_smart_startup_focus=1
let g:nerdtree_tabs_open_on_new_tab=1

" CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
"let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    \ 'file': '\v\.(exe|so|dll)$',
    \ 'link': 'some_bad_symbolic_links',
    \ }
" Other
filetype plugin on
nmap <F8> :TagbarToggle<CR>
nmap <leader>a :A<CR>
nmap <leader>v :AV<CR>
nnoremap <leader>jd :YcmCompleter GoTo<CR>
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
iab DC Daniel Collis <mayazcherquoi@gmail.com>
iab CCLOG CCLOG("CCLog:
let g:color_coded_enabled = 1
let g:color_coded_filetypes = ['c', 'cpp', 'h', 'cxx', 'hh', 'hpp', 'objc']

" Vim Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#ctrlp#color_template = 'insert'
let g:airline#extensions#ctrlp#color_template = 'normal'
let g:airline#extensions#ctrlp#color_template = 'visual'
let g:airline#extensions#ctrlp#color_template = 'replace'
let g:airline_powerline_fonts = 1
set laststatus=2
let g:airline#extensions#tabline#fnamemod = ':t'
set hidden
	" Create new buffer
nmap <leader>T :enew<cr>
	" Next buffer
nmap <leader>l :bnext<CR>
	" Previous buffer
nmap <leader>p :bprevious<CR>
	" Close buffer
nmap <leader>q :bp <BAR> bd #<CR>
	" List all buffers
nmap <leader>bl :ls<CR>
let g:airline_theme='bubblegum'
