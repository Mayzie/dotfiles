#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

# For Cocos 2D-X integration
if [ -d "/opt/cocos2d-x" ]; then
	export PATH=/opt/cocos2d-x/tools/cocos2d-console/bin:/opt/cocos2d-x/tools/cocos2d-console/plugins/plugin_package:/usr/local/bin:${PATH}
	export NDK_ROOT=/opt/android-ndk
	export ANDROID_SDK_ROOT=/opt/android-sdk
	export ANT_ROOT=/usr/bin
fi

# For Python virtualenv
if [ -e "/usr/bin/virtualenvwrapper.sh" ]; then
	export WblacORKON_HOME=~/.virtualenvs
	source /usr/bin/virtualenvwrapper.sh
fi

source ~/.bash_aliases
