#
# ~/.bashrc
#

function find_gitroot {
	git rev-parse --show-toplevel 2> /dev/null
}

function create_vim {
	dir_vim="${HOME}/Templates/Vim"
	if [ $# -eq 2 ]; then
		file_syntastic="$dir_vim/$2/syntastic"
		file_ycm="$dir_vim/$2/ycm"
	else
		file_syntastic="$dir_vim/$1/syntastic"
		file_ycm="$dir_vim/$1/ycm"
	fi
	if [ ! -e $file_syntastic ] || [ ! -e $file_ycm ]; then
		>&2 echo "Unable to find configuration files"
		return 1
	else
		git_root=`find_gitroot`
		if [ -z $git_root ] || [ $HOME == $git_root ]; then
			PWD=`pwd`
			cat $file_syntastic > "$PWD/.syntastic_$1""_config"
			cat $file_ycm > "$PWD/.ycm_extra_conf.py"
		else
			cat $file_syntastic > "$git_root/.syntastic_$1""_config"
			cat $file_ycm > "$git_root/.ycm_extra_conf.py"
		fi
	fi
}

function prepare_vcocos {
	curr_dir=`pwd`
	path=`find_gitroot`
	if [ ! -d $path ] || [ $HOME == $git_root ]; then
		path=$curr_dir
	else
		if [ ! -d "$path/.git" ]; then
			path=$curr_dir
		fi
	fi

	cd $path
	bear make
	cd $curr_dir
}

# Alias list
alias ls='ls --color=auto'
alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'
alias gp='git rev-parse --show-toplevel'
alias ll='ls -al'

alias vc='create_vim c'
alias vcpp='create_vim cpp'
alias vcocos='prepare_vcocos && create_vim cpp cocos2dx'
alias vpy='create_vim python3'
alias vpy2='create_vim python2'
